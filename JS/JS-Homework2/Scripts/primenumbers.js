let m = +prompt("Enter \"m\" number please.");
let n = +prompt("Enter \"n\" number please.");

while (isNaN(m) || isNaN(n) || m >= n) {
    m = +prompt("Enter \"m\" number please.");
    n = +prompt("Enter \"n\" number please.");
}

for (let i = m; i < n; i++) {
    let isNatural = true;

    for (let j = m; j < i; j++) {
        if (i % j === 0) isNatural = false;
    }
    if (isNatural) console.log(i);
}
