
let num1 = +prompt('Please, enter first number.');
let num2 = +prompt('Please, enter second number');
let operation = prompt('Please, enter: plus, minus, times sign or division sign');
let result;

const calculation = function() {

while (isNaN(num1) || isNaN(num2) || num1 == '' || num2 == '') {
    num1 = +prompt('Oops! Please, enter first number again.');
}

while (operation != '+' && operation != '-' && operation != '*' && operation != '/') {
    operation = prompt('Oops! Please, enter: plus, minus, times sign or division sign again.');
}

switch(operation) {
    case '+': 
        result = num1 + num2;
        alert(`${num1}${operation}${num2}=${result}`);
        break;
        
    
    case '-':
        result = num1 - num2;
        alert(`${num1}${operation}${num2}=${result}`);
        break;
       

    case '*':
        result = num1 * num2;
        alert(`${num1}${operation}${num2}=${result}`);
        break;
        

    case '/':
        result = num1 / num2;
        alert(`${num1}${operation}${num2}=${result}`);
        break;
        
    
    default:
        alert('Oops! :( Somesthing went wrong. You need to fill in all the required fields again.');
}
}
calculation();








